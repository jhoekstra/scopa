(ns scopa-ccw.core
  (:use clojure.pprint)
  (:require [clojure.string :as string]))

;;;Simulation of the Scopa game
(use 'clojure.pprint)

;the score accross games
(def score (atom {:me 0 :enemy 0}))

;;ingame state
;cards on the deck
(defn new-deck
  "returns a new deck"
  []
  (shuffle (for [dek '("S" "R" "K" "H")
                 n (range 1 11)]
             (keyword (str dek n)))))

(defn valid?
  "checks if a move is valid"
  [{:keys [card steal]}]
  true)

(defn parse
  "Parses user input to move. "
  [card steal]
  (let [move {:card (keyword card)
              :steal nil}]
    (if (some? steal)
      (assoc move :steal (map keyword steal))
      move)))

(defn get-input!
  "Get user input. keep asking until valid. a move is {:card :S7 :steal '(:S7 :S8}"
  []
  (let [[card & steal] (string/split (read-line) #" ")
        move (parse card steal)]
    (if (valid? move)
        move
      (do
        (println "That's not a valid move, try again")
        (recur)))))

(defn execute
  "executes a move and returns the updated state elements. Returns '(hand pile table)"
  [{:keys [card steal]} hand pile table]
  (do 
    (println "Puts card" card)
    (let [updated-hand (disj hand card)
          updated-table (clojure.set/union table hand)]
      (if (some? steal)
        (do 
          (apply println "Takes cards" card steal)
          [updated-hand
           (apply conj pile steal) 
           (apply disj (disj updated-table card) steal)])
        [updated-hand pile updated-table]))))
       

(defn my-move
  "Prints the state to the user. gets a move. returns resulting state"
  [my-hand my-pile table]
  (do
    (pprint {:my-hand my-hand :table table})
    (execute (get-input!) my-hand my-pile table)))

(defn your-move
  "AI move"
  [my-hand my-pile table]
  (let [move {:card (first my-hand)
              :steal nil}]
    (if (valid? move)
      (execute move my-hand my-pile table))))

(defn update-score [my-pile your-pile])

(defn resupply
  "resupplies hands with cards. returns '(my-cards your-cards deck)"
  [[c1 c2 c3 c4 c5 c6 & deck]]
  (do
    (println "Cards please")
    (list #{c1 c2 c3} #{c4 c5 c6} deck)))

(defn game-on
  "Simulates a game"
  []
  (loop [deck (new-deck)
         my-hand #{}
         your-hand #{}
         my-pile #{}
         your-pile #{}
         table #{}]
    
    (do 
      (apply println "Cards on the table" table)

    ;if hands are empty
      (if (every? empty? [my-hand your-hand])

        ;if deck is also empty, terminate game
        (if (empty? deck)
          (update-score my-pile your-pile)

          ;else resupply hands
          (let [[my-hand your-hand deck] (resupply deck)]
            (recur deck my-hand your-hand my-pile your-pile table)))

        ;if I have more cards left
        (if (>= (count my-hand) (count your-hand))

          ;get my-move via user input
          (let [[my-hand my-pile table] (my-move my-hand my-pile table)]
            (recur deck my-hand your-hand my-pile your-pile table))

          ;else let the ai move
          (let [[your-hand your-pile table] (your-move your-hand your-pile table)]
            (recur deck my-hand your-hand my-pile your-pile table)))))))



